<?php

$EM_CONF['html_minifier'] = [
    'title' => 'HTML Minifier',
    'description' => 'This extension minifies the output of TYPO3 generated pages.',
    'category' => 'fe',
    'version' => '1.2.1',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearcacheonload' => 1,
    'author' => 'Dominik Weber',
    'author_email' => 'post@dominikweber.de',
    'author_company' => 'www.dominikweber.de',
    'constraints' => [
        'depends' => [
            'php' => '7.2.0-7.4.99',
            'typo3' => '9.0.0-10.99.99',
        ],
    ],
];
