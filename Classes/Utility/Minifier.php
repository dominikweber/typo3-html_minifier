<?php

namespace DominikWeber\HtmlMinifier\Utility;

use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;

class Minifier
{
    /**
     * configurationManager.
     *
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;

    /**
     * @var array
     */
    protected $settings;

    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->configurationManager = $this->objectManager->get(ConfigurationManagerInterface::class);
        $fullConfiguration = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT
        );
        if (isset($fullConfiguration['plugin.']['tx_htmlminifier.'])) {
            $typoScriptService = $this->objectManager->get(TypoScriptService::class);
            $this->settings = $typoScriptService->convertTypoScriptArrayToPlainArray($fullConfiguration['plugin.']['tx_htmlminifier.']['settings.']);
        }
    }

    /**
     * process.
     *
     * @param string $content html of current page
     *
     * @return string minified html
     */
    public function process($content)
    {
        if (isset($this->settings['ignorePids'])) {
            $pages = GeneralUtility::trimExplode(',', $this->settings['ignorePids']);
            if ($pages) {
                $pages = array_flip($pages);
                if (isset($pages[$GLOBALS['TSFE']->page['uid']])) {
                    return $content;
                }
            }
        }
        if (isset($this->settings['excludePageTypes'])) {
            $pageTypes = GeneralUtility::trimExplode(',', $this->settings['excludePageTypes']);
            $pageTypes = array_flip($pageTypes);
            if (isset($pageTypes[$GLOBALS['TSFE']->type])) {
                return $content;
            }
        }

        return $this->minify($content);
    }

    /**
     * minify.
     *
     * @param string $content
     *
     * @return string $content
     */
    protected function minify($content)
    {
        $content = preg_replace('%(?>[^\S ]\s*| \s{2,})(?=(?:(?:[^<]++| <(?!/?(?:textarea|pre)\b))*+)(?:<(?>textarea|pre)\b| \z))%ix', ' ', $content);

        return $content;
    }
}
